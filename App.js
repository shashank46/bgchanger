import React ,{useState} from 'react';


import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
} from 'react-native';

const App = () => {
  // const randomColor = 'rgb(32,0,126)';

  const [randomColor, setRandomColor] = useState("rgb(32,0,126)");
  const changeBG = () => {
    let color = 'rgb(' +
    Math.floor(Math.random() *256) + 
      ',' +
    Math.floor(Math.random() *256) + 
      ',' +
    Math.floor(Math.random() *256) +
    ')';

    setRandomColor(color);
  };

  const changeBGB = () => {

    setRandomColor("#000000");

  }

  return (
      <>
      <StatusBar backgroundColor = {randomColor} />
        <View style = {[ styles.container1, {backgroundColor: randomColor} ] } >
          <TouchableOpacity onPress = {changeBG}>
            <Text style = {styles.text}>TAP ME </Text>
            </TouchableOpacity>
        </View>
        <View style = {[ styles.container2, {backgroundColor: randomColor} ] } >
          <TouchableOpacity onPress = {changeBGB}>
            <Text style = {styles.text}>RESET </Text>
            </TouchableOpacity>
        </View>
      </>
  );
};



export default App;



const styles = StyleSheet.create({
  container1:{
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
    // backgroundColor: randomColor,
    
  },
  container2:{
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
    // backgroundColor: randomColor,
    
  },
  text: {
    fontSize: 30,
    backgroundColor: "#BB2CD9",
    paddingVertical: 10,
    paddingHorizontal: 40,
    color: "#FFFFFF",
    borderRadius:15,
    textTransform: "uppercase", 
  }
})